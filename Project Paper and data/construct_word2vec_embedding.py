from collections import Counter
import pandas as pd
import numpy as np

#Return a list of the 2 most common products that were purchased with product_id in 50 orders
def most_common_products_purchased_with_product(product_id):
	dict_of_product_ids_occurrences=Counter()
	product_rows=df[df['product_id']==product_id]
	length=len(product_rows)
	if length>0:
		product_rows=product_rows.sample(n=min(50,length))
	for index, row in product_rows.iterrows():
		order_num=row['order_number']
		user_id=row['user_id']
		order_id1 = orders[orders['user_id']==user_id]
		order_id = order_id1[order_id1['order_number']==order_num]   
		products_in_order = prior[prior['order_id']==order_id['order_id'].unique()[0]]['product_id']
		for r in products_in_order.iteritems():
			if r[1] != product_id:
				dict_of_product_ids_occurrences[r[1]]+=1;    

	return (dict_of_product_ids_occurrences.most_common(2))

if __name__ == '__main__':
	
	df = pd.read_csv('DF.csv')
		 
	word_col=np.repeat(np.arange(1,num_of_products), 4)
	context_col = []
	label_col = []

	for product_id in range(1,num_of_products):
		positive=most_common_products_purchased_with_product(product_id)
		if len(positive)==0:
			index = np.argwhere(word_col==product_id)
			word_col = np.delete(word_col, index)
		else:
			context_col.append(positive[0][0])
			label_col.append(1)
			if len(positive)>1:
				context_col.append(positive[1][0])
				label_col.append(1)
			else:
				index = np.argwhere(word_col==product_id)
				word_col = np.delete(word_col, index[0])
				print('product_id - '+str(product_id)+' has only one positive example!')
			context_col.append(random.randint(1, 49688))
			label_col.append(0)
			context_col.append(random.randint(1, 49688))
			label_col.append(0)

	word2vec_table = pd.DataFrame({ 'word' : word_col, 'context' : context_col, 'label' : label_col}, columns=['word', 'context', 'label'])
		
	word2vec_table.to_csv('word2vec_table.csv')