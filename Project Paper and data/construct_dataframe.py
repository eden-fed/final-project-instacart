import numpy as np
import pandas as pd
import time
from collections import defaultdict

def orderId2orderNum(x):
    return(orderDictionary[x]['order_number'].unique()[0])
def orderId2userId(x):
    return(orderDictionary[x]['user_id'].unique()[0])
def orderId2orderDow(x):
    return(orderDictionary[x]['order_dow'].unique()[0])
def orderId2orderHour(x):
    return(orderDictionary[x]['order_hour_of_day'].unique()[0])
def orderId2orderDays(x):
    days = orderDictionary[x]['days_since_prior_order'].unique()[0]
    if np.isnan(days):
        return 400
    else:
        return days
def checkProductInNextOrder(order,productId):
    return orderToProductList[nextOrderDictionary[order]][productId]



if __name__ == '__main__':
	order_products__prior = pd.read_csv('D:\\DataScience\\instacart_2017_05_01\\order_products__prior.csv')
	order_products__train = pd.read_csv('D:\\DataScience\\instacart_2017_05_01\\order_products__train.csv')
	orders = pd.read_csv('D:\\DataScience\\instacart_2017_05_01\\orders.csv')
	print('finsih loading csv files')
	order_products__prior = order_products__prior.drop('add_to_cart_order', axis=1)
	order_products__prior = order_products__prior.drop('reordered', axis=1)
	
	for i in range(325):
		it = i
		startPoint = i * 100000
		endPoint = (i + 1) * 100000
		test = order_products__prior[startPoint:endPoint]
		start = time.time()
		currentOrders = test['order_id'].unique()
		orderDictionary={}
		nextOrderDictionary={}
		productsInOrder={}
		orderToProductList={}
		for i in currentOrders:
			orderDictionary[i] = orders[orders['order_id']==i]
			a = orderDictionary[i].index[0] 
			nextOrderDictionary[i] = orders.loc[a+1,'order_id']
			file = orders[orders['order_id']==nextOrderDictionary[i]]['eval_set'].unique()[0]
			if file == 'prior':
				d = dict.fromkeys(order_products__prior[order_products__prior['order_id']==nextOrderDictionary[i]]['product_id'].unique(),1)
			else:
				d = dict.fromkeys(order_products__train[order_products__train['order_id']==nextOrderDictionary[i]]['product_id'].unique(),1)  
			orderToProductList[nextOrderDictionary[i]] = defaultdict(lambda: 0, d)  
		print('finish build dictionary!')    
		test['order_number'] = test['order_id'].apply(orderId2orderNum)
		test['user_id'] = test['order_id'].apply(orderId2userId)
		test['order_dow'] = test['order_id'].apply(orderId2orderDow)
		test['order_hour'] = test['order_id'].apply(orderId2orderHour)
		test['order_days_since_prior'] = test['order_id'].apply(orderId2orderDays)
		test['future_buy'] = test.apply(lambda row: checkProductInNextOrder(row['order_id'], row['product_id']), axis=1)
		test.to_csv('export' + str(it) + '.csv')
		end = time.time()
		print(it, 'iteartion done. Time:')
		print(end - start)
